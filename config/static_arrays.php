<?php
/**
 * StaticArray
 * 
 * @created    22/09/2018
 * @package    Ak Clinics
 * @copyright  Copyright (C) 2018
 * @license    Proprietary
 * @author     Mohit Thakur
 */
Class StaticArray
{
   public static $enquire_types = array(
       1 => "Hair Transplant",
       2 => "Cosmetic Surgery",
       3 => "Cosmetology"
   );
}

