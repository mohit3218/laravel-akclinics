<?php

/**
* Functions
* 
* 
* @created    22/09/2018
* @package    Ak Clinics
* @copyright  Copyright (C) 2018
* @license    Proprietary
* @author     Mohit Thakur
*/

/**
* 
* @param type $data
*/
function debug($data)
{
   $bt = debug_backtrace();
   $caller = array_shift($bt);
   
   echo "<pre>";
   echo "<b>" . $caller["file"] . " : " . $caller["line"] . "</b><br/>";
   print_r($data);
   echo "</pre>";
}

/**
* 
* @param type $logs
* @return type
*/
function get_query_log($logs)
{
   foreach ($logs as $k => $log)
   {
       if ($log["bindings"])
       {
           foreach($log["bindings"] as $v)
           {
               $pos = strpos($log["query"], "?");
               
               if ($pos !== false) {
                   $log["query"] = substr_replace($log["query"], $v, $pos, 1);
               }
           }
           
           $logs[$k]["query"] = $log["query"];
       }
   }
   
   return $logs;
}