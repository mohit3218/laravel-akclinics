<?php

/**
 * API Routes
 * 
 * @created    22/09/2018
 * @package    Ak Clinics
 * @copyright  Copyright (C) 2018
 * @license    Proprietary
 * @author     Mohit Thakur
 * 
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route to create a new role
Route::post('role', 'Auth\JwtAuthenticateController@createRole');
// Route to create a new permission
Route::post('permission', 'Auth\JwtAuthenticateController@createPermission');
// Route to attache permission to a role
Route::post('attach-permission', 'Auth\JwtAuthenticateController@attachPermission');
// Route to assign role to user
Route::post('assign-role', 'Auth\JwtAuthenticateController@assignRole');

//Login route
Route::post('users/login', 'UserController@login');

//Cors implement for all routes login users
Route::group(['prefix' => 'admin', 'middleware' => ['cors','ability:admin,admin']], function()
{
	//User routes
	Route::get('users', 'UserController@index');
	Route::post('users', 'UserController@store');
	Route::get('users/show/{id}', 'UserController@show');
	Route::put('users/{user}', 'UserController@update');
	Route::delete('users/{user}', 'UserController@destroy');
	Route::put('users/changePassword/{user}', 'UserController@changePassword'); 
	Route::put('users/toggle/{user}/{status}', 'UserController@toggle');
	
	//Pages routes
	Route::get('pages', 'PageController@index');
	Route::post('pages', 'PageController@store');
	Route::get('pages/{id}', 'PageController@show');
	Route::put('pages/{page}', 'PageController@update');
	Route::delete('pages/{page}', 'PageController@destroy');
	Route::put('pages/toggle/{page}/{status}', 'PageController@toggle');
	Route::get('pages/list/pagelist', 'PageController@pageList');
    
    //Contact routes
	Route::get('contacts', 'ContactController@index');
	Route::post('contacts', 'ContactController@store');
	Route::delete('contacts/{contact}', 'ContactController@destroy');
    
    //Slug routes
    Route::get('slugs', 'SlugController@index');
    Route::post('slugs', 'SlugController@store');
	Route::get('slugs/{id}', 'SlugController@show');
	Route::put('slugs/{slug}', 'SlugController@update');
	Route::delete('slugs/{slug}', 'SlugController@destroy');
    Route::get('slugs/list/sluglist', 'SlugController@slugList');
    Route::get('slugs/list/slugalllist', 'SlugController@slugAllList');
});

//Routes Visitors screens
Route::group(['middleware' => ['cors']], function()
{
	//Contact routes
	Route::post('contacts', 'ContactController@contactAdd');
    
    Route::get('pages/pageRecord/{id}', 'PageController@pageRecord');
});