<?php
/**
 * Contact Model
 * 
 * @created    22/09/2018
 * @package    Ak Clinics
 * @copyright  Copyright (C) 2018
 * @license    Proprietary
 * @author     Mohit Thakur
 */
namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends AppModel
{
	use SoftDeletes;
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'contacts';
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','email','enquire_types','enquire_types','phone','subject','message','city','country'];
	
	/**
	* The attributes that should be mutated to deleted_at.
	*
	* @var array
	*/
    protected $deleted_at = ['deleted_at'];
}
