<?php

/**
 * User Model
 * 
 * @created    22/09/2018
 * @package    Ak Clinics
 * @copyright  Copyright (C) 2018
 * @license    Proprietary
 * @author     Mohit Thakur
 */

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends AppModel implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{
    use Notifiable, Authenticatable, CanResetPassword, EntrustUserTrait;

	//For ignore EntrustUserTrait delete and restore function
	use SoftDeletes { SoftDeletes::restore insteadof EntrustUserTrait; }
	
	/**
	* The database table used by the model.
	*
	* @var string
	*/
    protected $table = 'users';
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
	
	/**
	* 
	* @param type $password
	*/
	public function setPasswordAttribute($password)
	{   
		$this->attributes['password'] = bcrypt($password);
	}
	
	/**
	* The attributes that should be mutated to deleted_at.
	*
	* @var array
	*/
    protected $deleted_at = ['deleted_at'];
}
