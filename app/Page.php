<?php
/**
 * Page Model
 * 
 * @created    22/09/2018
 * @package    Ak Clinics
 * @copyright  Copyright (C) 2018
 * @license    Proprietary
 * @author     Mohit Thakur
 */

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends AppModel
{
	use SoftDeletes;
	/**
	* The table associated with the model.
	*
	* @var string
	*/
    protected $table = 'pages';
	
    /**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
    protected $fillable = ['name', 'slug_id','meta_title','meta_keywords', 'meta_description', 'og_title' , 'og_description', 'og_image', 'is_active'];
	
	/**
	* The attributes that should be mutated to deleted_at.
	*
	* @var array
	*/
    protected $deleted_at = ['deleted_at'];
}
