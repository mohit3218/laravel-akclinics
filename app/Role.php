<?php
/**
 * Role Model
 * 
 * @created    22/09/2018
 * @package    Ak Clinics
 * @copyright  Copyright (C) 2018
 * @license    Proprietary
 * @author     Mohit Thakur
 */

namespace App;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
}