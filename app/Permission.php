<?php
/**
 * Permission Model
 * 
 * @created    22/09/2018
 * @package    Ak Clinics
 * @copyright  Copyright (C) 2018
 * @license    Proprietary
 * @author     Mohit Thakur
 */

namespace App;

use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission
{
}