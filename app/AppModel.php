<?php
/**
 * App Model
 * 
 * @created    22/09/2018
 * @package    Ak Clinics
 * @copyright  Copyright (C) 2018
 * @license    Proprietary
 * @author     Mohit Thakur
 */

namespace App;

use Illuminate\Support\Facades\Schema;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use JWTAuth;

abstract class AppModel extends \Illuminate\Database\Eloquent\Model
{
    public static $authUser = [];
    
	//format for saving in database
    protected $dateFormat = "Y-m-d H:i:s";
	
    protected $createdBy = true, $updatedBy = true, $updatedAt = true, $isActive = true;

    /**
     * Listen for event
     */
    protected static function boot()
    {
		try
        {
			// Get token
			$token = JWTAuth::getToken();
		
			//Set token to check
			JWTAuth::setToken($token);
			
			// Get JWT Auth user id with verify the JWT Auth Token
            self::$authUser = JWTAuth::authenticate()->id;
        } 
        catch (TokenExpiredException $e)
        {
			return response()->json(['status' => 'tymon.jwt.expired', 'token_expired', 'data'=>$e->getStatusCode(), [$e]]);
        }
        catch (JWTException $e)
        {
			return response()->json(['status' => 'tymon.jwt.invalid', 'token_invalid', 'data'=>$e->getStatusCode(), [$e]]);
        }
		
		//extending Illuminate\Database\Eloquent\Model class, it is calling the boot method for that Model class.
        parent::boot();
		
		static::saving(function($model)
        {
            return $model->beforeSave($model);
        });
        
        static::updating(function($model)
        {
            return $model->beforeSave($model);
        });
    }
    
    /**
     * event before save
     * @return bool
     */
    protected function beforeSave($model)
    {
		// $this->exists is return if record is exist or not
		if ($this->exists)
        {
			// Checked the column name in database
			if(Schema::hasColumn($model->table, 'updated_by'))
			{
				//Set auth user id for updated by field
				$this->setAttribute("updated_by", self::$authUser);
			}
        }
        else
        {
			// Checked the column name in database
			if(Schema::hasColumn($model->table, 'created_by'))
			{
				//Set auth user id for created_by field
				$this->setAttribute("created_by", self::$authUser);
			}
			
			// Checked the column name in database
			if(Schema::hasColumn($model->table, 'is_active'))
			{
				//Set is_active = 1 for all add records
				$this->setAttribute("is_active", 1);
			}
       }
        return true;
    }
}