<?php
/**
 * JwtAuthenticate Middleware
 * 
 * @created    22/09/2018
 * @package    Ak Clinics
 * @copyright  Copyright (C) 2018
 * @license    Proprietary
 * @author     Mohit Thakur
 */

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Middleware\BaseMiddleware;

class TokenEntrustAbility extends BaseMiddleware
{
	// Check for all route request permission and role
    public function handle($request, Closure $next, $roles, $permissions, $validateAll = false)
    {
		//Checked Token exsist or not 
        if (!$token = $this->auth->setRequest($request)->getToken())
        {
            return $this->respond('tymon.jwt.absent', 'token_not_provided', 400);
        }

		//Get the user detail with token
        try
        {
            $user = $this->auth->authenticate($token);
        } 
        catch (TokenExpiredException $e)
        {
			//return when token expried
            return $this->respond('tymon.jwt.expired', 'token_expired', $e->getStatusCode(), [$e]);
        }
        catch (JWTException $e)
        {
			//return when token invalid
            return $this->respond('tymon.jwt.invalid', 'token_invalid', $e->getStatusCode(), [$e]);
        }

		//Checked user exist or not
        if (!$user)
        {
            return $this->respond('tymon.jwt.user_not_found', 'user_not_found', 404);
        }

		// Checked the route permission
        if (!$request->user()->ability(explode('|', $roles), explode('|', $permissions), array('validate_all' => $validateAll)))
        {
            return $this->respond('tymon.jwt.unauthorized', 'Unauthorized Access', 401);
        }

        $this->events->fire('tymon.jwt.valid', $user);

        return $next($request);
    }

}
