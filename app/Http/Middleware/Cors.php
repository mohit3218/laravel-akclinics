<?php
/**
 * Cors
 * 
 * @created    22/09/2018
 * @package    Ak Clinics
 * @copyright  Copyright (C) 2018
 * @license    Proprietary
 * @author     Mohit Thakur
 */

namespace App\Http\Middleware;

use Closure;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: Content-Type, Accept, Authorization, X-Requested-With, Application');
        return $next($request);
    }
}
