<?php
/**
 * Page Controller
 * 
 * @created    22/09/2018
 * @package    Ak Clinics
 * @copyright  Copyright (C) 2018
 * @license    Proprietary
 * @author     Mohit Thakur
 */

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use App\Page;
use App\PageSection;
use Validator;

class PageController extends AppController
{
	//Set for public Model name
	public $modelName = "Page";
	
    //Define validation rules for the request
	public $rules = array(
        'name' => 'required',
        'slug_id' => 'required',
        'meta_title' => 'required',
        'meta_keywords' => 'required',
        'meta_description' => 'required'
    );
	/**
	* Display a listing of the resource.
	*
	* @return \Illuminate\Http\Response
	*/
    public function index()
    {
		$conditions = $this->getSearchConditions([
            ["view_field" => "name", "type" => "string"],
            ["view_field" => "slug", "type" => "string"]
        ]);
		
		if($conditions)
		{
			//Paginate page according to search filter
			$records = Page::whereRaw($conditions)->orderBY(Input::get('sf'), Input::get('so'))->paginate(PAGINATION_LIMIT);
		}
		else 
		{
			//Paginate page without search filter
			$records = Page::orderBy(Input::get('sf'), Input::get('so'))->paginate(PAGINATION_LIMIT);
		}
        
        return response()->json(['status' => 1,  'data' => $records]);
	}
	
	/**
	* Adds new record
	* @return type
	*/
    public function store()
    {
		//Call parent method for add the user page
		return parent::add_record();
	}
	
	/**
	* Show the form for editing the specified resource.
	*
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/
    public function show($id)
    {
		$pages = Page::findOrFail($id);
		
		return response()->json(['status' => 1 ,"data" => $pages]);
    }
	
	/**
	* Update the specified resource in storage.
	* @param Page $page
	* @return type
	*/
    public function update(Page $page)
    {
		//Call parent method foe update the user details
		return parent::edit_record($page);
	}
	
	/**
	* Deletes record
	* @param Page $page
	* @return type
	*/
    public function destroy(Page $page)
    {
		///Call a delete action in main controller to delete a record
		return parent::delete_record($page);
    }
	
	/**
	* Display a listing of the Page.
	*
	* @return \Illuminate\Http\Response
	*/
    public function pageList()
    {
		$records = Page::where('is_active', 1)->pluck('name', 'id')->toArray();
		
		return response()->json(['status' => 1, 'data' => $records]);
	}
	
	/**
	* Status update Existing Record
	* @param type Page $page, $status
	*/
    public function toggle(Page $page, $status) 
	{
        //Call a toggleStatus action in main controller to update a status
        return parent::toggleStatus($page, $status);
    }
    
    public function pageRecord($id)
    {
        $records = Page::where('id', $id)->first();
		
		return response()->json(['status' => 1, 'data' => $records]);
    }
}