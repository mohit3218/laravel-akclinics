<?php
/**
 * JwtAuthenticate Controller
 * 
 * @created    22/09/2018
 * @package    Ak Clinics
 * @copyright  Copyright (C) 2018
 * @license    Proprietary
 * @author     Mohit Thakur
 */

namespace App\Http\Controllers\Auth;

use App\Permission;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JwtAuthenticateController extends Controller
{
	//Create a Role for login user
    public function createRole(Request $request)
    {
        $role = new Role();
        $role->name = $request->input('name');
		$role->display_name = $request->input('display_name');
		$role->description = $request->input('description');
        $role->save();
		
        return response()->json("created");
    }

	//Create a Permission for access routes
    public function createPermission(Request $request)
    {
        $permission = new Permission();
        $permission->name = $request->input('name');
		$permission->display_name = $request->input('display_name');
		$permission->description = $request->input('description');
        $permission->save();

        return response()->json("created");
    }

	//Assigned a permission for role
	public function attachPermission(Request $request)
    {
        $role = Role::where('name', '=', $request->input('role'))->first();
        $permission = Permission::where('name', '=', $request->input('name'))->first();
        $role->attachPermission($permission);

        return response()->json("Attached   ");
    }
	
	//Assigned a role for user
    public function assignRole(Request $request)
    {
        $user = User::where('email', '=', $request->input('email'))->first();

        $role = Role::where('name', '=', $request->input('role'))->first();
        
		$user->roles()->attach($role->id);

        return response()->json("Assigned");
    }
}