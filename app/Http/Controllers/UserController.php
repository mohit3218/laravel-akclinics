<?php
/**
 * User Controller
 * 
 * @created    22/09/2018
 * @package    Ak Clinics
 * @copyright  Copyright (C) 2018
 * @license    Proprietary
 * @author     Mohit Thakur
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use JWTAuth;
use DB;
use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;

class UserController extends AppController
{
	public $modelName = "User";

	//Define validation rules for the request
	public $rules = array(
        'name' => 'required',
		'email' => 'required|email|unique:users',
		'password' => 'required|min:6'
    );
	
    /**
	* Display a listing of the resource.
	*
	* @return \Illuminate\Http\Response
	*/
    public function index()
    {
		$conditions = $this->getSearchConditions([
            ["view_field" => "name", "type" => "string"]
        ]);
		
		if($conditions)
		{
			//Paginate page according to search filter
			$records = User::whereRaw($conditions)->where('id', '!=' ,JWTAuth::authenticate()->id)->paginate(PAGINATION_LIMIT);
		}
		else 
		{
			//Paginate page without search filter
			$records = User::where('id', '!=' ,JWTAuth::authenticate()->id)->paginate(PAGINATION_LIMIT);
		}
        
        return response()->json(['status' => 1,  'data' => $records]);
	}

    /**
	* Adds new record
	* @return type
	*/
    public function store()
    {
		$data = Input::all();
		
		//Use laravel Validator helper and $this->rule is represented the which input field you have to mandatory and numaric
        $validator = Validator::make($data, $this->rules);
        
		if ($validator->passes())
        {
			$user = new User();
			$user->name = $data['name'];
			$user->email = $data['email'];
			$user->password = $data['password'];
			
			DB::beginTransaction(); //Start transaction!
			try
			{
			   //saving logic here
			   $user->save();
               
               $user->roles()->attach(1);
			}
			catch(\Exception $e)
			{
			  //failed logic here
			   DB::rollback();
			   return response()->json(['status' => 0,'message'=>$e->getMessage()]);
			}
			DB::commit();
			
			return response()->json(['status' => 1,'message' => 'User has been created successfully!']);
		}
		else
		{
			//return response()->json(['status' => 1,'data'=>$validator->errors()]);
			return response()->json(['status' => 0,'message'=>'Unable to create user']);
		}
	}
	
	/*
	 * User Login
	 * * @param  \Illuminate\Http\Request  $request
	 */
	public function login(Request $request) 
	{
		$credentials = $request->only('email', 'password');
        
		try 
		{
            $user = User::where('email', $credentials['email'])->first();
			
			if($user != null)
			{
				$role = DB::table('users')->select('role_user.role_id')
					->join('role_user', 'role_user.user_id', '=', 'users.id')->where('user_id', $user['id'])->get();

				$user['role'] = $role[0]->role_id;
			}
			
			// verify the credentials and create a token for the user
			if (!$token = JWTAuth::attempt($credentials, ['user' => $user])) 
            {
				return response()->json(['message' => 'Invalid username & password'], 200);
			}
		} 
		catch (JWTException $e) 
		{
			// something went wrong
			return response()->json(['message' => 'could_not_create_token'], 200);
		}
		// if no errors are encountered we can return a JWT
		return response()->json(['status' => 1,'token' => $token]);
	}

	/**
	* Show the form for editing the specified resource.
	*
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/
    public function show($id)
    {
		$users = User::findOrFail($id);
		
		return response()->json(['status' => 1 ,"data" => $users]);
    }
	
    /**
	* Update the specified resource in storage.
	* @param User $user
	* @return type
	*/
    public function update(User $user)
    {
		//For update change email validation rule with user id
		$this->rules['email'] = $this->rules['email'] . ',id,' . $user->id;
		
		//Call parent method foe update the user details
		return parent::edit_record($user);
	}

    /**
	* Deletes record
	* @param User $user
	* @return type
	*/
    public function destroy(User $user)
    {
		///Call a delete action in main controller to delete a record
		return parent::delete_record($user);
    }
	
	/**
	* Change password
	* @param type User $user 
	*/
	public function changePassword(User $user)
	{
		//Get request data in data valiable
		$data = request()->all();
		
		//Set request old password value in variable
		$oldPassword = $data['oldpassword'];
		
		//Set request new password value in variable with hash
		$newPassword = Hash::make($data['newpassword']);
		
		//Cofirm hash password with request old password value
		if(Hash::check($oldPassword, $user->password))
		{
			//find the user with user id and update the password
			 $user::where('id', '=', $user->id)->update(['password' => $newPassword]);
			 //Sent success response
			 return response()->json(['status' => 1,'message' => 'Password has been updated successfully!']);
		}
		else
		{
			//Sent failure response due to some error
			return response()->json(['status' => 0,'message' => 'Old password didn\'t match' ]); 
		}
	}
	
	/**
	* Status update Existing Record
	* @param type User $user, $status
	*/
    public function toggle(User $user, $status) 
	{
        //Call a toggleStatus action in main controller to update a status
        return parent::toggleStatus($user, $status);
    }
}