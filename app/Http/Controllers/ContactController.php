<?php
/**
 * Contact Controller
 * 
 * @created    22/09/2018
 * @package    Ak Clinics
 * @copyright  Copyright (C) 2018
 * @license    Proprietary
 * @author     Mohit Thakur
 */

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;

use App\Contact;

class ContactController extends AppController
{
	// Set for public Model name
	public $modelName = "Contact";
	
	//Define validation rules for the request
	public $rules = array(
        'name' => 'required',
        'email' => 'required',
        'phone' => 'required|numeric|digits:10',
        'city' => 'required',
        'country' => 'required',
        'enquire_types' => 'required',
    );
	
	/**
	* Display a listing of the resource.
	*
	* @return \Illuminate\Http\Response
	*/
    public function index()
    {
		$conditions = $this->getSearchConditions([
            ["view_field" => "name", "type" => "string"],
            ["view_field" => "enquire_types", "type" => "integer"],
        ]);
		
		if($conditions)
		{
			//Paginate page according to search filter
			$records = Contact::whereRaw($conditions)->orderBY(Input::get('sf'), Input::get('so'))->paginate(PAGINATION_LIMIT);
		}
		else 
		{
			//Paginate page without search filter
			$records = Contact::orderBy(Input::get('sf'), Input::get('so'))->paginate(PAGINATION_LIMIT);
		}
        
        return response()->json(['status' => 1,  'data' => $records]);
	}
	
	/**
	* Adds new record
	* @return type
	*/
    public function store()
    {
		//Call parent method for add the Contact details
		return parent::add_record();
	}
	
	/**
	* Deletes record
	* @param Contact $contact
	* @return type
	*/
    public function destroy(Contact $contact)
    {
		///Call a delete action in main controller to delete a record
		return parent::delete_record($contact);
    }
	
	/**
	* Adds new record for Visitors
	* @return type
	*/
    public function contactAdd()
    {
		//Call parent method for add the Contact details
		return parent::add_record();
	}
}
