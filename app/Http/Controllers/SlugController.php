<?php
/**
 * Slug Controller
 * 
 * @created    29/09/2018
 * @package    Ak Clinics
 * @copyright  Copyright (C) 2018
 * @license    Proprietary
 * @author     Mohit Thakur
 */

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;

use App\Slug;
use DB;

class SlugController extends AppController
{
	// Set for public Model name
	public $modelName = "Slug";
	
	//Define validation rules for the request
	public $rules = array(
        'name' => 'required'
    );
	
	/**
	* Display a listing of the resource.
	*
	* @return \Illuminate\Http\Response
	*/
    public function index()
    {
		$conditions = $this->getSearchConditions([
            ["view_field" => "name", "type" => "string"],
        ]);
		
		if($conditions)
		{
			//Paginate page according to search filter
			$records = Slug::whereRaw($conditions)->orderBY(Input::get('sf'), Input::get('so'))->paginate(PAGINATION_LIMIT);
		}
		else 
		{
			//Paginate page without search filter
			$records = Slug::orderBy(Input::get('sf'), Input::get('so'))->paginate(PAGINATION_LIMIT);
		}
        
        return response()->json(['status' => 1,  'data' => $records]);
	}
	
	/**
	* Adds new record
	* @return type
	*/
    public function store()
    {
		//Call parent method for add the Contact details
		return parent::add_record();
	}
    
    /**
	* Show the form for editing the specified resource.
	*
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/
    public function show($id)
    {
		$slugs = Slug::findOrFail($id);
		
		return response()->json(['status' => 1 ,"data" => $slugs]);
    }
	
	/**
	* Update the specified resource in storage.
	* @param Slug $slug
	* @return type
	*/
    public function update(Slug $slug)
    {
		//Call parent method foe update the slug details
		return parent::edit_record($slug);
	}
	
	/**
	* Deletes record
	* @param Contact $contact
	* @return type
	*/
    public function destroy(Slug $slug)
    {
		///Call a delete action in main controller to delete a record
		return parent::delete_record($slug);
    }
    
    /**
	* Display a listing of the Slug.
	*
	* @return \Illuminate\Http\Response
	*/
    public function slugList()
    {
		$records = Slug::all(['id', 'name']);
		
		return response()->json(['status' => 1, 'data' => $records]);
	}
    
    /**
	* Display a listing of the slugs.
	*
	* @return \Illuminate\Http\Response
	*/
    public function slugAllList()
    {
		$records = Slug::where('is_active', 1)->pluck('name', 'id')->toArray();
		
		return response()->json(['status' => 1, 'data' => $records]);
	}
}
