<?php
/**
 * Slug Model
 * 
 * @created    29/09/2018
 * @package    Ak Clinics
 * @copyright  Copyright (C) 2018
 * @license    Proprietary
 * @author     Mohit Thakur
 */
namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;

class Slug extends AppModel
{
	use SoftDeletes;
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'slugs';
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','is_active'];
	
	/**
	* The attributes that should be mutated to deleted_at.
	*
	* @var array
	*/
    protected $deleted_at = ['deleted_at'];
}
