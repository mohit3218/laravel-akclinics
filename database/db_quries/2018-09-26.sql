DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `message` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

ALTER TABLE `contacts` ADD COLUMN `deleted_at` TIMESTAMP NULL AFTER `message`;

ALTER TABLE `contacts` ADD COLUMN `subject` VARCHAR(100) DEFAULT NULL AFTER `phone`;

ALTER TABLE `contacts` ADD COLUMN `city` VARCHAR(100) NOT NULL AFTER `message`;

ALTER TABLE `contacts` ADD COLUMN `country` VARCHAR(100) NOT NULL AFTER `city`;

ALTER TABLE `contacts` ADD COLUMN `enquire_types` int(10) NOT NULL AFTER `email`;