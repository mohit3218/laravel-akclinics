ALTER TABLE `pages` 
CHANGE COLUMN `name` `name` VARCHAR(5000) NOT NULL ,
CHANGE COLUMN `meta_title` `meta_title` VARCHAR(5000) NULL ,
CHANGE COLUMN `meta_keywords` `meta_keywords` VARCHAR(5000) NULL DEFAULT NULL ,
CHANGE COLUMN `og_title` `og_title` VARCHAR(6500) NOT NULL ,
CHANGE COLUMN `og_description` `og_description` VARCHAR(6000) NOT NULL ;
